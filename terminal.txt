#Here are some terminal commands

#Activating the terminal:
Linux - start the terminal app
Mac - use the search (magnifying glass) and enter "Terminal"
Windows - Enter "cmd" (Windows terminal has few useful capabilities compared to Mac and Linux)
	Use git bash to get a better terminal experience for computer programming


cd  Change Directory Command
########################################
cd .. 				#Change to the Previous Directory
cd someFolder  			#Change to someFolder
cd /  				#Change to the root directory (Like c:/ on Windows)
cd ~  				#Change to the home directory



ls List Command
########################################
ls -la 				#List all files and their attributes

pwd 				#Present Working Directory - where are we in the computer?
# file system?

mkdir Make a directory
########################################
mkdir someFolder    		#Make a directory called 'someFolder'

mv  				#Move a file or change a file name
mv preview.py J05preview.py  	#Change the file name from preview.py to J05preview.py
mv preview.py classNotes	#Move preview.py into the classNotes folder
mv preview.py ../classNotes     #Move preview.py one folder up and then into the classNotes folder

vim  				#Terminal editor in Linux (and Mac!!)
nano				#Terminal editor for Linux (and Mac!!)



 
