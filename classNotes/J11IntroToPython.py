#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 10 21:00:13 2021
Notes - Introduction to Python
@author: wneal
"""

#Comments
#Find two different types of comment
#Single line comment - use a hashtag
a = 5 #In-line comment
'''
This is a multi-line comment
It may span multiple lines of text
It begins and ends with three single quotation marks.
'''

"""
This is a multi-line comment
It may span multiple lines of text
It begins and ends with three double quotation marks.
"""


#Variables
#Find four different types of variable
name = "Ted" #text
a = 6 #integer - positive or negative number without a decimal
answer = False #Boolean (True or False)
number = 25.443 #floating point (number with a decimal)



#Arithmetic
#What are the arithmetic operators in Python?
#Two types: unary and binary
#Unary:  operations only on one operand 
x = -7  #Negative sign is a unary operator as it only modifies 7
#Binary: require two operands
#6+4:  The plus sign (+) is a binary operator
#It requires two operands: in this case, 6 and 4
a = 47*5 #multiply with '*'
b = 47/5 #divide with '/'
b_int = 47//5 #Two division signs - integer division
c = 47+5 #add with '+'
d = 47-5 #subtract with '-'


#Explain order of operations in Python
#Python follows mathematical rules
#Enter and evaluate an algebraic or numeric expression
(4+5)*3-4*8+20/4
result = a*b-d*d/a


#Identify at least three types of operator and provide examples
#of each

#Operators

#Assignment Operators
#These operators set variables to particular values
#   '=' - This is the standard assignment operator
a = 5
print(a)
a += 5  #Adds 5 to a
print(a)
a -= 5  #Subtracts 5 from a
print(a)
a *=5   #Multiplies a by 5
print(a)
a+=1
a /=5   #Divides a by 5 (floating point)
print(a)
a = 26
a //= 5 #Divides a by 5 (integer division)
print(a)

#Comparison Operators
#These operators compare two quantities and return True or False
a = 53
b = 74
print(a==b)  #Double equal sign "==" - compares two values
print(a>b) #Greater than
print(a<b) #Less than
print(a>=b) #Greater than or equal to
print(a<=b) #Less than or equal to 
print(a!=b) #Not equal to



#Boolean Operators - These are logical operators
# and, or, not
#  example - if a > 50 or b < 100:  Logical 'or'
# if a > 50 and b < 100:   Logical 'and'
# if not(a == 5):  Logical not



#Input and Output
#Demonstrate how to collect user input
answer = input("What is your favourite colour?")  #This is an input statement
print("You answered: ",answer)



#Demonstrate how to print (output)