#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 14 20:44:48 2021
J14 - Lists, Tuples, Dictionaries
@author: wneal
"""


a = 5
b = [4,6,4,7,4,3,7,4,7,4,7,4]  #This is a list of numbers.  Use [ ] (square brackets)

print(b)

#We use an index to access a particular item in the list:
#print(b[0])
#print(b[1])
#print(b[2])
#print(b[3])
#print(b[4])

#If you see a set of statements repeating, chances are you should use a loop
for i in range(0,12):
    print(b[i])
    
    
#Lists are mutable:
#We can change the items in a list
b[3] = 123
print(b)   


#We can access ranges of values in a list:
print(b[3:7]) # A range of values
print(b[6:])  #From a value and beyond
print(b[:8])  #Up to but not including a value
print(b[-1])   #Print the last item in the list
print(b[-2])    #Print the second last item in the list

#We can add to or remove items from a list
b.append(789) #This "method" adds an item to the list
print(b)
b.pop(3)
print(b)
print(b.index(7)) #What is the index of the first instance of 7 in the list?
#print(b.index(2345))   #This produces an error as the number is not in the list

#Lists can add and multiply in a particular way.
print(b+b+b)
print(b*5)

#What are tuples?
#Tuples are immutable lists - they cannot change
#We define tuples with round brackets
c = (4,6,4,7,4,3,7,4,7,4,7,4)  #This is a tuple 

#We can access ranges of values in a list:
print(c[3:7]) # A range of values
print(c[6:])  #From a value and beyond
print(c[:8])  #Up to but not including a value
print(c[-1])   #Print the last item in the list
print(c[-2])    #Print the second last item in the list

#c[3] = 3456  #This does not work because tuples are immutable.


#What are dictionaries?
d = {}  #This is an empty dictionary
d["Canada"] = "Ottawa"
d["China"] = "Beijing"
d["Nigeria"] = "Abuja"
d["Russia"] = "Moscow"
print(d)
print(d["Canada"]) #We use dictionaries by providing a "key"
#The dictionary returns a value
print(list(d.keys()))
print(list(d.values()))


#A1BasicsOfPython.py
#Upload a file of that name to your folder in your repository



colours = ["Red","Green","Yellow","Blue","Brown","Black","White","Orange"]
print(colours.index("Purple"))





