#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 20:49:21 2021
Decision Structures
@author: wneal
"""

'''
#Review a few concepts
a = 5
answer = input("How old are you?")
t = 50/6
u = 50//6
#if a==5 and b==6:
#   pass
    #    ==  vs. =
#What are True and False?
#How do we describe the print statement?
#print is the output statement
The print statement is followed by round brackets.
Inside the brackets is a comma-separated (, , ,)  list of items to print
Variables, numbers and text may be included in this list.


'''

# if statements are used in making decisions in Python

#Input a variable, answer
answer = input("What is the capital city of Canada?")
#Based on the value of answer, provide a particular response


#Simple if structure
if answer == "Ottawa":  #if....variable....comparison....variable....:
    print("You are correct!") #Conditional lines are "tabbed"
    
'''
#if..else structure:
if answer == "Ottawa":  #if....variable....comparison....variable....:
    print("You are correct!") #Conditional lines are "tabbed"
else:
    print("You are not correct! Please try again!")

'''

#if..elif..else structure:
if answer == "Ottawa":  #if....variable....comparison....variable....:
    print("You are correct!") #Conditional lines are "tabbed"
elif answer == "Toronto":
    print("Incorrect! That is the largest city in Canada!")
elif answer == "Vancouver":
    print("Incorrect! That is the largest city in British Columbia!")
else:
    print("You are not correct! Please try again!")


#Dealing with case-sensitivity
text1 = "Ottawa"
text2 = text1.lower()
text3 = text1.upper()
print(text1,text2,text3)




